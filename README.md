# Gulp shik
Version 2.5

Fronend base template

Created by Ilya Komichev
http://ilko.me


## Instalation
- Install node.js http://nodejs.org/

- Install Gulp globaly 
```sh
	npm install gulp -g
```

- Open project folder and update modules
```sh
npm update
```

## Update
```sh
npm update
```


## Run project
In project folder run
```sh
gulp
```

## Description
- http://localhost:8080
- See more in gulpfile.js



### Sctructure
- gulpfile.js Gulp settings
- gulp-run.cmd Run Gulp (only for Windows users)
- site compiled site
- source sourses of jade and stylus